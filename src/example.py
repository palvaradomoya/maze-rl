
"""example.py: Use a maze and solve it with classic wall-following
   algorithm.

   The RL versions should be faster
"""

__author__ = "Pablo Alvarado"
__copyright__ = "Copyright 2020, Pablo Alvarado"
__license__ = "BSD 3-Clause License (Revised)"

import time
import math
import random
import dispatcher
import env


class ExampleStepper:
    """Simple stepper performing a random walk"""

    def __init__(self, dispatch):
        self.dispatch = dispatch
        self.reset()
        self.init()
        self.lastRenderingTime = time.perf_counter()
        
    def reset(self):
        """Account for posible serialization of new envirnoments"""
        print("ExampleStepper.reset() called")

        # Do this to get a valid reference to the environment in use
        self.env = self.dispatch.env()

        # You can for instance get the number of cells in the maze with:
        self.xCells = self.env.maze.nx
        self.yCells = self.env.maze.ny

        # Or also get the size of a cell
        self.cellSizeX = self.env.maze.cellSizeX
        self.cellSizeY = self.env.maze.cellSizeY

        # Here an arbitrary step size for the "advance" action
        step = self.env.maze.cellSizeX/3

        # Sample 50/50 a rotation or a translation
        self.actions = [self.env.agent.right,
                        self.env.agent.left,
                        lambda: self.env.agent.advance(step),
                        lambda: self.env.agent.advance(step)]
        
    def init(self):
        """Reset and setup the stepper"""

        print("ExampleStepper.init() called")

        # Tell the environment to place the agent at the beginning
        self.env.reset()

        # Ensure the agent's position is properly displayed
        self.dispatch.render()
        self.lastRenderingTime = time.perf_counter()

    cardinalPoints = ['E', 'S', 'W', 'N', 'E']

    def step(self, iteration):
        """Perform one simulation step"""

        self.env.tryAction(self.env.agent, random.choice(self.actions))
        
        # How to extract the agent's position and orientation
        apx = self.env.agent.state.posX
        apy = self.env.agent.state.posY
        aa = self.env.agent.state.angle

        # How to determine which cell the agent is in
        cx = math.floor(apx/self.env.maze.cellSizeX)
        cy = math.floor(apy/self.env.maze.cellSizeY)
        
        # Which direction the agent is facing?
        dir = math.floor((aa+45)/90)

        # Only at most 15fps:
        # - Print information about current position 
        # - Draw the agent's position and (if activated) the sensors
        if (time.perf_counter() - self.lastRenderingTime) > 1/15:
            print("ExampleStepper.step(", iteration, ")")
            print("  Agent continous state: ", self.env.agent)
            print("  Agent discrete state: ({},{})@{}".\
                  format(cx,
                         cy,
                         self.cardinalPoints[dir]))
            self.dispatch.render()
            self.lastRenderingTime = time.perf_counter()
        
        # Check which is the finishing cell
        finX, finY = self.env.maze.endX, self.env.maze.endY

        # Check if the agent as reached the finish cell
        if self.env.finished():
            print("GOAL REACHED!")
            self.dispatch.pause()
            # self.dispatch.restart()  # We could restart instead!
        


# #######################
# # Main control center #
# #######################

# This object centralizes everything
theDispatcher = dispatcher.Dispatcher()

# Provide a new environment (maze + agent)
theDispatcher.setEnvironment(env.Environment(12, 8, 15))

# Provide also the simulation stepper, which needs access to the
# agent and maze in the dispatcher.
theDispatcher.setStepper(ExampleStepper(theDispatcher))

# Start the GUI and run it until quit is selected
# (Remember Ctrl+\ forces python to quit, in case it is necessary)
theDispatcher.run()
